#! /bin/sh
if [ "${S3_S3V4}" = "true" ]
then
  aws configure set default.s3.signature_version s3v4
fi

if [ "${S3_ACCESS_KEY_ID}" = "" ]
then
  echo "You need to set the S3_ACCESS_KEY_ID environment variable."
  exit 1
fi

if [ "${S3_SECRET_ACCESS_KEY}" = "" ]
then
  echo "You need to set the S3_SECRET_ACCESS_KEY environment variable."
  exit 1
fi

if [ "${S3_BUCKET}" = "" ]
then
  echo "You need to set the S3_BUCKET environment variable."
  exit 1
fi

if [ "${S3_ENDPOINT}" = "" ]
then
  AWS_ARGS=""
else
  AWS_ARGS="--endpoint-url ${S3_ENDPOINT}"
fi

if [ "${SQLITE_FILENAME}" = "" ]
then
  echo "You need to set the SQLITE_FILENAME environment variable."
  exit 1
fi

export AWS_ACCESS_KEY_ID=$S3_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY=$S3_SECRET_ACCESS_KEY
export AWS_DEFAULT_REGION=$S3_REGION

echo "Removing db-backup directory..."
rm -rf $SQLITE_DIRECTORY/db-backup

echo "Creating db-backup directory..."
mkdir $SQLITE_DIRECTORY/db-backup

echo "Creating backup of ${SQLITE_FILENAME} database..."

sqlite3 $SQLITE_DIRECTORY/$SQLITE_FILENAME ".backup '$SQLITE_DIRECTORY/db-backup/$SQLITE_FILENAME'"

gzip $SQLITE_DIRECTORY/db-backup/$SQLITE_FILENAME

echo "Uploading sqlite backup to bucket $S3_BUCKET"

cat $SQLITE_DIRECTORY/db-backup/$SQLITE_FILENAME.gz | aws $AWS_ARGS s3 cp - s3://$S3_BUCKET/$S3_PREFIX/${SQLITE_FILENAME}_$(date +"%Y-%m-%dT%H:%M:%SZ").gz || exit 2

echo "Removing db-backup directory..."
rm -rf $SQLITE_DIRECTORY/db-backup

echo "Database backup uploaded successfully"
exit 0