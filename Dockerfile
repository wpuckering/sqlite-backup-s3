FROM alpine:3.9
LABEL maintainer="William Puckering <william.puckering@gmail.com>"

RUN apk add --no-cache \
    python \
    py2-pip \
    sqlite \
  && pip install awscli \
  && apk del py2-pip

ENV SQLITE_DIRECTORY ''
ENV SQLITE_FILENAME ''
ENV S3_SECRET_ACCESS_KEY ''
ENV S3_BUCKET ''
ENV S3_REGION 'us-west-1'
ENV S3_PREFIX ''
ENV S3_ENDPOINT ''
ENV S3_S3V4 'true'

ADD run.sh run.sh

CMD ["sh", "run.sh"]