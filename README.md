# sqlite-backup-s3

A simple container for backing up a SQLite database to a bucket on an Amazon S3-compatible object storage server. Built with the intention of running as a CronJob in Kubernetes.